package DataAccess;

import Model.Order;
import Connection.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

/**
 * Create OrderDAO class from AbstractDAO
 */
public class OrderDAO extends AbstractDAO<Order> {
    public OrderDAO(){
        super();
    }

    /**
     * Find the object by the specified type (Client/Product) from the database
     * @param type Based on what we are searching
     * @return the found object
     */
    public List<Order> findByName(String type,String name){
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        String query =createSelectNameQuery(type);
        try {
            connection = DBConnection.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet=statement.executeQuery();
            List<Order> list=createObjects(resultSet);
            if(list.isEmpty())
                return null;
            return list;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, Order.class.getName()+"DAO:findByName "+e.getMessage());
        }finally {
            DBConnection.close(resultSet);
            DBConnection.close(statement);
            DBConnection.close(connection);
        }
        return null;
    }
}
