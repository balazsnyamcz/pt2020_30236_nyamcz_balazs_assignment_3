package DataAccess;

import Model.Client;

/**
 * Create ClientDAO class from AbstractDAO
 */
public class ClientDAO extends AbstractDAO<Client>{
    public ClientDAO(){
       super();
    }
}
