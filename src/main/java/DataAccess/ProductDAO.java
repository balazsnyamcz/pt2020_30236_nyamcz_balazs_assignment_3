package DataAccess;

import Model.Product;

/**
 * Create ProductDAO class from AbstractDAO
 */
public class ProductDAO extends AbstractDAO<Product> {
    public ProductDAO(){
        super();
    }
}
