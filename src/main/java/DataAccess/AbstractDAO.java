package DataAccess;

import Connection.DBConnection;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * AbstractDAO class defines the common operations for accessing a table: Insert,
 * Update, FindByName, FindAll. Define the operations on the specified generic type <T>
 * @param <T>
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER=Logger.getLogger(AbstractDAO.class.getName());
    private final Class<T> type;

    /**
     * For each AbstractDAO object obtain the class of the generic type T
     */
    @SuppressWarnings("unchecked")
    public AbstractDAO(){
        this.type=(Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Create a Select by name query based on the parameter
     * @param name the key/name
     * @return the Select Statement
     */
    protected String createSelectNameQuery(String name){
        return "SELECT " +
                " * " +
                " FROM " +
                "warehouse."+
                type.getSimpleName() +
                " WHERE " + name + "=?" + " AND deleted=false";
    }

    /**
     * Create a Select all query
     * @return the Select Statement
     */
    private String createSelectAllQuery(){
        return "SELECT "+
                " * "+
                " FROM "+
                "warehouse."+
                type.getSimpleName()+
                " WHERE deleted=false";
    }

    /**
     * Create an Insert query
     * @return Insert Statement
     */
    private String createInsertQuery(){
        StringBuilder string= new StringBuilder();
        string.append("INSERT ");
        string.append(" INTO ");
        string.append("warehouse.");
        string.append(type.getSimpleName());
        string.append(" VALUES (");
        int length=type.getDeclaredFields().length;
        for (int i=0;i<length-1;i++) {
            string.append(" ? , ");
        }
        string.append(" ? )");
        return string.toString();
    }

    /**
     * Create an update query
     * @param name the name/key
     * @return update statement
     */
    private String createUpdateQuery(String name){
        StringBuilder string= new StringBuilder();
        string.append("UPDATE ");
        string.append("warehouse.");
        string.append(type.getSimpleName());
        string.append(" SET ");
        for (Field field: type.getDeclaredFields()) {
            field.setAccessible(true);
            string.append(field.getName());
            string.append("= ? ,");
        }
        string.deleteCharAt(string.lastIndexOf(","));
        string.append(" WHERE ");
        string.append(name);
        string.append("=?;");
        return string.toString();
    }

    /**
     * Given a result set, obtain the list of model objects of type T
     * @param resultSet the result of a query
     * @return A list of objects (T)
     */
    protected List<T> createObjects(ResultSet resultSet){
        List<T> list=new ArrayList<T>();
        try {
            while (resultSet.next()){
                T instance = type.getConstructor().newInstance();
                for (Field field: type.getDeclaredFields()) {
                    Object value=resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor=new PropertyDescriptor(field.getName(),type);
                    Method method=propertyDescriptor.getWriteMethod();
                    method.invoke(instance,value);
                }
                list.add(instance);
            }
        } catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException | InvocationTargetException | SQLException | IntrospectionException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Find the object by the specified name from the database
     * @param name Based on what we are searching
     * @return the found object
     */
    public T findByName(String name){
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        String query =createSelectNameQuery(type.getDeclaredFields()[0].getName());
        try {
            connection = DBConnection.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet=statement.executeQuery();
            List<T> list=createObjects(resultSet);
            if(list.isEmpty())
                return null;
            return list.get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName()+"DAO:findByName "+e.getMessage());
        }finally {
            DBConnection.close(resultSet);
            DBConnection.close(statement);
            DBConnection.close(connection);
        }
        return null;
    }

    /**
     * Find all the objects from the database
     * @return a list of objects
     */
    public List<T> findAll(){
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        String query =createSelectAllQuery();
        try {
            connection = DBConnection.getConnection();
            statement = connection.prepareStatement(query);
            resultSet=statement.executeQuery();
            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName()+"DAO:findAll "+e.getMessage());
        }finally {
            DBConnection.close(resultSet);
            DBConnection.close(statement);
            DBConnection.close(connection);
        }
        return null;
    }

    /**
     * Insert into the database an object given as a parameter
     * @param t the object
     * @return the inserted object
     */
    public T insert(T t){
        Connection connection=null;
        PreparedStatement statement=null;
        String query=createInsertQuery();
        int i=1;
        try {
            connection=DBConnection.getConnection();
            statement=connection.prepareStatement(query);
            for (Field field: t.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                Type aux=field.getType();
                if(aux.equals(String.class)) {
                    statement.setString(i++,(String)field.get(t));
                } else if (aux.equals(int.class)) {
                    statement.setInt(i++,(int) field.get(t));
                }else if (aux.equals(boolean.class)) {
                    statement.setBoolean(i++, (boolean) field.get(t));
                }else if (aux.equals(float.class)){
                    statement.setFloat(i++,(float) field.get(t));
                }
            }
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName()+" DAO:insert "+ e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }finally {
            DBConnection.close(statement);
            DBConnection.close(connection);
        }
        return t;
    }

    /**
     * Update the given onject in the database
     * @param t the object
     * @return the updated object
     */
    public T update(T t){
        Connection connection =null;
        PreparedStatement statement=null;
        String query=createUpdateQuery(t.getClass().getDeclaredFields()[0].getName());
        try {
            connection=DBConnection.getConnection();
            statement=connection.prepareStatement(query);
            int i=1;
            for (Field field: t.getClass().getDeclaredFields()) {
                field.setAccessible(true);

                Type aux=field.getType();
                if(aux.equals(String.class)) {
                    statement.setString(i++,(String)field.get(t));
                } else if (aux.equals(int.class)) {
                    statement.setInt(i++,(int) field.get(t));
                }else if (aux.equals(boolean.class)) {
                    statement.setBoolean(i++, (boolean) field.get(t));
                }else if (aux.equals(float.class)){
                    statement.setFloat(i++,(float) field.get(t));
                }
            }
            Field field=t.getClass().getDeclaredFields()[0];
            field.setAccessible(true);
            statement.setString(i,(String)field.get(t));
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName()+"DAO:update "+ e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }finally {
            DBConnection.close(statement);
            DBConnection.close(connection);
        }
        return t;
    }
}
