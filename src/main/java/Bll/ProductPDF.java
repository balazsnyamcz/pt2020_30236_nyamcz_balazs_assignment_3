package Bll;

import Model.Product;

import java.util.List;

/**
 * Create ProductPDF class from PDFGenerator
 */
public class ProductPDF extends PDFGenerator<Product> {
    public ProductPDF(String pdfName, List<Product> list) {
        super(pdfName, list);
    }
}
