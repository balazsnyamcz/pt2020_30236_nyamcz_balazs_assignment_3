package Bll;

import DataAccess.OrderDAO;
import DataAccess.ProductDAO;
import Model.Order;
import Model.Product;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Encapsulate the application logic for Products
 */
public class ProductBll {
    private ProductDAO productDAO;

    public ProductBll(){
        productDAO=new ProductDAO();
    }

    /**
     * Shows all products
     */
    public void reportProduct(){
        List<Product> products=productDAO.findAll();
        if(products== null){
            //TODO:Generate not found pdf
            System.out.println("There are no products!");
        }else {
            Date date=new Date();
            SimpleDateFormat dateFormat=new SimpleDateFormat("hh_mm_ss_SSS");
            ProductPDF productPDF=new ProductPDF("src/main/resources/ReportProduct_"+ dateFormat.format(date)+".pdf",products);
            productPDF.generateReport();
            System.out.println(products);
        }
    }

    /**
     * Create and insert a Product using a list of data
     * @param data list of data
     */
    public void insertProduct(ArrayList<String> data){
        Product product=productDAO.findByName(data.get(0));
        Product insertedProduct;
        if(product==null){
            product=new Product();
            product.setName(data.get(0));
            product.setQuantity(Integer.parseInt(data.get(1)));
            product.setPrice(Float.parseFloat(data.get(2)));
            product.setDeleted(false);
            insertedProduct=productDAO.insert(product);
            //TODO: generate inserted pdf
        }else {
            int db=product.getQuantity() + Integer.parseInt(data.get(1));
            product.setQuantity(db);
            insertedProduct=productDAO.update(product);
            //TODO: generate already exist pdf
        }
        System.out.println(insertedProduct);
    }

    /**
     * Delete the product which corresponds to the list of data
     * @param data list of data
     */
    public void deleteProduct(ArrayList<String> data){
        Product product=productDAO.findByName(data.get(0));
        Product deletedProduct;
        OrderDAO orderDAO=new OrderDAO();
        List<Order> orders;
        if(product==null){
            //TODO: Generate not exist pdf
            deletedProduct=null;
        }else{
            orders= orderDAO.findByName(Order.class.getDeclaredFields()[1].getName(),data.get(0));
            if(orders!=null) {
                for (Order order : orders) {
                    order.setDeleted(true);
                }
            }
            product.setDeleted(true);
            deletedProduct=productDAO.update(product);
            //TODO: Generate deleted pdf
        }
        System.out.println(deletedProduct);
    }
}
