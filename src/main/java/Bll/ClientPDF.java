package Bll;

import Model.Client;

import java.util.List;

/**
 * Create ClientPDF class from PDFGenerator
 */
public class ClientPDF extends PDFGenerator<Client> {
    public ClientPDF(String pdfName, List<Client> list) {
        super(pdfName, list);
    }
}
