package Bll;

import DataAccess.ClientDAO;
import DataAccess.OrderDAO;
import Model.Client;
import Model.Order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Encapsulate the application logic for Clients
 */
public class ClientBll {
    private ClientDAO clientDAO;

    public ClientBll(){
        clientDAO=new ClientDAO();
    }

    /**
     * Shows all the clients
     */
    public void reportClient(){
        List<Client> clients=clientDAO.findAll();
        if(clients== null){
            //TODO:Generate not found pdf
            System.out.println("There are no cients!");
        }else {
            //TODO:Generate found pdf
            Date date=new Date();
            SimpleDateFormat dateFormat=new SimpleDateFormat("hh_mm_ss_SSS");
            ClientPDF clientPDF=new ClientPDF("src/main/resources/ReportClient_"+ dateFormat.format(date)+".pdf",clients);
            clientPDF.generateReport();
            System.out.println(clients);
        }
    }

    /**
     * Create and insert a Client using a list of data
     * @param data list of data
     */
    public void insertClient(ArrayList<String> data){
        Client client=clientDAO.findByName(data.get(0));
        Client insertedClient;
        if(client==null){
            client=new Client();
            client.setName(data.get(0));
            client.setAddress(data.get(1));
            client.setDeleted(false);
            insertedClient=clientDAO.insert(client);
            //TODO:Generate inserted pdf
        }else {
            insertedClient=client;
            //TODO: Generate already exist pdf
        }
        System.out.println(insertedClient);
    }

    /**
     * Delete the client which corresponds to the list of data
     * @param data list of data
     */
    public void deleteClient(ArrayList<String> data){
        Client client=clientDAO.findByName(data.get(0));
        Client deletedClient;
        OrderDAO orderDAO=new OrderDAO();
        List<Order> orders;
        if(client==null){
            //TODO: Generate not exist pdf
            deletedClient=null;
        }else{
            orders= orderDAO.findByName(Order.class.getDeclaredFields()[0].getName(),data.get(0));
            if(orders!=null) {
                for (Order order : orders) {
                    order.setDeleted(true);
                }
            }
            client.setDeleted(true);
            deletedClient=clientDAO.update(client);
            //TODO: Generate deleted pdf
        }
        System.out.println(deletedClient);
    }
}
