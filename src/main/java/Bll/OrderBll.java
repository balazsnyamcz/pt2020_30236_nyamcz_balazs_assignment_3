package Bll;

import DataAccess.ClientDAO;
import DataAccess.OrderDAO;
import DataAccess.ProductDAO;
import Model.Client;
import Model.Order;
import Model.Product;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *  Encapsulate the application logic for Orders
 */
public class OrderBll {
    private OrderDAO orderDAO;

    public OrderBll(){
        orderDAO=new OrderDAO();
    }

    /**
     * Shows all orders
     */
    public void reportOrder(){
        List<Order> orders=orderDAO.findAll();
        if(orders==null){
            //TODO:Generate not found pdf
            System.out.println("There are no orders!");
        }else {
            Date date=new Date();
            SimpleDateFormat dateFormat=new SimpleDateFormat("hh_mm_ss_SSS");
            OrderPDF orderPDF=new OrderPDF("src/main/resources/ReportOrder_"+ dateFormat.format(date)+".pdf",orders);
            orderPDF.generateReport();
            System.out.println(orders);
        }
    }

    /**
     * Create and insert an Order using a list of data
     * @param data list of data
     */
    public void insertOrder(ArrayList<String> data){
        Order insertedOrder=null;
        ClientDAO clientDAO=new ClientDAO();
        Client client=clientDAO.findByName(data.get(0));
        if(client!=null){
            ProductDAO productDAO=new ProductDAO();
            Product product=productDAO.findByName(data.get(1));
            if (product!=null){
                int db=product.getQuantity();
                if (db>=Integer.parseInt(data.get(2))){
                    OrderDAO orderDAO=new OrderDAO();
                    Order order=new Order();
                    order.setClient(data.get(0)); order.setProduct(data.get(1)); order.setQuantity(Integer.parseInt(data.get(2)));
                    order.setPrice(Float.parseFloat(data.get(2))*product.getPrice()); order.setDeleted(false);
                    insertedOrder=orderDAO.insert(order);
                    product.setQuantity(db-Integer.parseInt(data.get(2)));
                    productDAO.update(product);
                    Date date=new Date();
                    SimpleDateFormat dateFormat=new SimpleDateFormat("hh_mm_ss_SSS");
                    OrderPDF orderPDF=new OrderPDF("src/main/resources/Order_"+ dateFormat.format(date)+".pdf");
                    orderPDF.bll(insertedOrder);
                }else{
                    Date date=new Date();
                    SimpleDateFormat dateFormat=new SimpleDateFormat("hh_mm_ss_SSS");
                    OrderPDF orderPDF=new OrderPDF("src/main/resources/Order_"+ dateFormat.format(date)+".pdf");
                    orderPDF.bll(insertedOrder);
                }
            }else{/*TODO:Product not in db report*/}
        }else {/*TODO: Client not in db report*/}
        System.out.println(insertedOrder);
    }
}
