package Bll;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.stream.Stream;

/**
 * PDFGenerator class defines the common operations for generating a pdf.
 * Define the operations on the specified generic type <T>
 * @param <T>
 */
public class PDFGenerator<T> {
    /**
     * The name of the pdf
     */
    private String pdfName;
    private final Class<T> type;
    private List<T> list;

    @SuppressWarnings("unchecked")
    public PDFGenerator(String pdfName, List<T> list) {
        this.pdfName = pdfName;
        this.type=(Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.list=list;
    }

    public PDFGenerator(String pdfName, Class<T> type) {
        this.pdfName=pdfName;
        this.type = type;
    }

    /**
     * Generate a pdf with a table
     */
    public void generateReport() {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(pdfName));
            document.open();

            PdfPTable table = new PdfPTable(type.getDeclaredFields().length);
            addTableHeader(table);
            addRows(table);

            document.add(table);
            document.close();
        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Add a header to the table
     * @param table
     */
    private void addTableHeader(PdfPTable table){
        Stream.of(type.getDeclaredFields()).forEach(field->{
            PdfPCell header=new PdfPCell();
            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
            header.setBorderWidth(2);
            header.setPhrase(new Phrase(field.getName()));
            table.addCell(header);
        });
    }

    /**
     * Add rows to the table
     * @param table
     */
    private void addRows(PdfPTable table){
        for (T t:list) {
            for (Field field:t.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    table.addCell(field.get(t).toString());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Get the Pdf name
     * @return pdf name
     */
    public String getPdfName() {
        return pdfName;
    }
}
