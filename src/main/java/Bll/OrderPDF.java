package Bll;

import Model.Order;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

/**
 * Create OrderPDF class from PDFGenerator
 */
public class OrderPDF extends PDFGenerator<Order> {
    public OrderPDF(String pdfName, List<Order> list) {
        super(pdfName, list);
    }
    public OrderPDF(String pdfName){
        super(pdfName,Order.class);

    }

    /**
     * Generate a pdf from an order
     * @param order the order
     */
    public void bll(Order order){
        try {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(getPdfName()));
        document.open();

        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Paragraph paragraph;
        if(order!=null) {
            paragraph = new Paragraph(order.getClient()+" a comanadat " + order.getQuantity()+ " bucata din " + order.getProduct()+ ". Trebuie sa plăteasca " + order.getPrice() +" RON.", font);
        }else{
            paragraph=new Paragraph("Nu exista suficiente produse in stoc.",font);
        }
        document.add(paragraph);
        document.close();
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
