package Model;

/**
 * Order class uses for storing order information.
 */
public class Order {

    /**
     * name of a client who ordered
     */
    private String client;

    /**
     * name of a product what was ordered
     */
    private String product;

    /**
     * the number of the ordered products
     */
    private int quantity;

    /**
     * the price of the ordered products
     */
    private float price;

    /**
     *  shows that if an order is deleted or not from the database
     */
    private boolean deleted;

    /**
     * This is a constructor to initialize an order object.
     */
    public Order(){
    }

    /**
     * This is a constructor to initialize an order object.
     * @param client an initial client name
     * @param product an initial product name
     * @param quantity an initial number of products
     * @param price an initial price of the product
     * @param deleted initial order status
     */
    public Order(String client, String product, int quantity, float price, boolean deleted) {
        this.client = client;
        this.product = product;
        this.quantity = quantity;
        this.price = price;
        this.deleted = deleted;
    }

    /**
     * Get client name
     * @return client name
     */
    public String getClient() {
        return client;
    }

    /**
     * To set a name of a client
     * @param client a new client name
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * Get product name
     * @return product name
     */
    public String getProduct() {
        return product;
    }

    /**
     * To set a name of a product
     * @param product a new product name
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * Get the number of ordered products
     * @return number of ordered products
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * To set the number of ordered products
     * @param quantity a new number of ordered products
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Get the price of the ordered products
     * @return price of the ordered products
     */
    public float getPrice() {
        return price;
    }

    /**
     * To set the price of the ordered products
     * @param price a new price of the ordered products
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * Get order status
     * @return order status
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * To set a status for an order (deleted/not deleted)
     * @param deleted a new status
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * Show order information
     * @return order information
     */
    @Override
    public String toString() {
        return client +" " + product + " " + quantity + " " + price + " " + deleted + "\n";
    }
}
