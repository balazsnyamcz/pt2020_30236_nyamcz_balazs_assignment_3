package Model;

/**
 * Product class uses for storing product information.
 */
public class Product {

    /**
     * name of a product
     */
    private String name;

    /**
     * the number of the products
     */
    private int quantity;

    /**
     * the price of the  products
     */
    private float price;

    /**
     *  shows that if a product is deleted or not from the database
     */
    private boolean deleted;

    /**
     * This is a constructor to initialize a product object.
     */
    public Product(){
    }

    /**
     * This is a constructor to initialize a product object.
     * @param name an initial product name
     * @param quantity an initial number of products
     * @param price an initial price of the product
     * @param deleted initial product status
     */
    public Product(String name,  int quantity, float price, boolean deleted) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.deleted = deleted;
    }

    /**
     * Get product name
     * @return product name
     */
    public String getName() {
        return name;
    }

    /**
     * To set a name of a product
     * @param name a new product name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the price of the products
     * @return price of the products
     */
    public float getPrice() {
        return price;
    }

    /**
     * To set the price of the products
     * @param price a new price of the products
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * Get the number of the products
     * @return number of the products
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * To set the number of  products
     * @param quantity a new number of  products
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Get product status
     * @return product status
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * To set a status for a product (deleted/not deleted)
     * @param deleted a new status
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * Show product information
     * @return product information
     */
    @Override
    public String toString() {
        return name +" "+ quantity + " "+price + " " + deleted +"\n";
    }
}
