package Model;

/**
 * Client class uses for storing client information.
 */

public class Client {
    /**
     * name of a client.
     */
    private String name;

    /**
     * address of a client.
     */
    private String address;

    /**
     * shows that if a client is deleted or not from the database
     */
    private boolean deleted;

    /**
     * This is a constructor to initialize client object.
     */
    public Client(){
    }

    /**
     *This is a constructor to initialize client object.
     * @param name an initial client name
     * @param address an intitial client address
     * @param deleted initial client status
     */
    public Client(String name, String address, boolean deleted) {
        this.name = name;
        this.address = address;
        this.deleted = deleted;
    }

    /**
     * Get client name
     * @return client name
     */
    public String getName() {
        return name;
    }

    /**
     * To set a name of a client
     * @param name a new client name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get client address
     * @return client address
     */
    public String getAddress() {
        return address;
    }

    /**
     * To set an address for a client
     * @param address a new address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Get client status
     * @return client status
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * To set a status for a client (deleted/not deleted)
     * @param deleted a new status
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * Show client information
     * @return client information
     */
    @Override
    public String toString() {
        return name + " " + address + " " + deleted ;
    }
}
