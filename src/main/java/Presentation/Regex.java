package Presentation;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Regex class used to divide strings into substrings based on a certain key
 */
public class Regex {

    /**
     * A compiled representation of a regex.
     */
    private Pattern checkRegex;

    /**
     * This is a constructor to initialize the regex with the key (theRegex)
     * @param theRegex a string of  metacharacters and literal characters
     */
    public Regex(String theRegex) {
        this.checkRegex = Pattern.compile(theRegex);
    }

    /**
     * Divide/split the string parameter into substrings
     * @param command the string which would be splitted
     * @return a list of substrings
     */
    public ArrayList<String> splitCommand( String command) {
        ArrayList<String> splittedCommand=new ArrayList<>();
        Matcher regexMatcher = checkRegex.matcher(command);
        while (regexMatcher.find()){
            splittedCommand.add(regexMatcher.group().trim());
        }
        return splittedCommand;
    }
}
