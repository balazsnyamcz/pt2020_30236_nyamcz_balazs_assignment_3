package Presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * CommandReader class used to process the data from the input
 */
public class CommandReader {

    /**
     * Reads line-by-line from the input file get as a parameter and process them by using regex class and commandParser class
     * @param fileName the name of the input file
     */
  public void readCommands(String fileName){
        File file=new File(fileName);
        Scanner input;
        try {
            input=new Scanner(file);
            Regex regex=new Regex("[0-9a-zA-Z .-]+");
            ArrayList<String> data;
            CommandParser commandParser=new CommandParser();
            while (input.hasNextLine()){
                String command=input.nextLine();
                data=regex.splitCommand(command);
                System.out.println(data);
                commandParser.parser(data);
            }
            input.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
