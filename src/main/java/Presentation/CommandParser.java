package Presentation;

import Bll.ClientBll;
import Bll.OrderBll;
import Bll.ProductBll;

import java.util.ArrayList;

/**
 * CommandParser class used to decide based on the given commands which class/method from the business logic layer would be used
 */
public class CommandParser {

    /**
     * Verify based on first parameter which method from the ClientBll class would be used
     * @param action Command name
     * @param data a command
     */
    private void clientCase(String action, ArrayList<String> data){
        ClientBll clientBll=new ClientBll();
        switch (action){
            case "Insert":
                clientBll.insertClient(data);
                break;
            case "Delete":
                clientBll.deleteClient(data);
                break;
            case "Report":
                clientBll.reportClient();
                break;
        }
    }

    /**
     * Verify based on first parameter which method from the ProductBll class would be used
     * @param action command name
     * @param data a command
     */
    private void productCase(String action, ArrayList<String> data){
        ProductBll productBll=new ProductBll();
        switch (action){
            case "Insert":
                productBll.insertProduct(data);
                break;
            case "Delete":
                productBll.deleteProduct(data);
                break;
            case "Report":
                productBll.reportProduct();
                break;
        }
    }

    /**
     * Based on the parameter choose the coresponding method to be performed
     * @param data a command
     */
    public void parser(ArrayList<String> data){
        Regex regex=new Regex("[a-zA-Z]+");
        ArrayList<String> commands;
        commands=regex.splitCommand(data.get(0));
        data.remove(0);
        if(commands.size()>1) {
            switch (commands.get(1)) {
                case "client":
                case "Client":
                    clientCase(commands.get(0), data);
                    break;
                case "product":
                case "Product":
                    productCase(commands.get(0), data);
                    break;
                case "order":
                    OrderBll orderBll=new OrderBll();
                    orderBll.reportOrder();
                    break;
            }
        }else {
            if(commands.get(0).equals("Order")){
                OrderBll orderBll=new OrderBll();
                orderBll.insertOrder(data);
            }
        }
    }
}
