package Start;

import Presentation.CommandReader;

/**
 * Main class used for running the appl.
 */
public class Main {

    /**
     * @param args The command line parameters
     */
    public static void main(String[] args){
        CommandReader commandReader=new CommandReader();
        commandReader.readCommands(args[0]);

    }
}
